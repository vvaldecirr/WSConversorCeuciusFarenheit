Projeto reestruturado


PARA TESTAR:
1-Rode como "Java Aplication" a classe "Publicador" do pacote "control";
2-Configure como desejar testar e rode como "Java Aplication" a classe "Programa" do pacote "cliente";
    
    *OBS.: Lembre-se que o teste da classe Programa pode ser feito quantas vezes precisar, 
    mas para cada altera��o nas classes do pacote controle � neces�rio parar o servi�o do console
    e rodar novamente como "Java Aplication".


WSGEN exemplo:
wsgen -cp bin -s src -wsdl control.Echo

WSDL test:
http://localhost:10000/echo?wsdl

WSIMPORT exemplo:
wsimport -s src -d bin -p cliente http://localhost:10000/echo?wsdl