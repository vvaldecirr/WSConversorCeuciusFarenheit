
package control.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "conversor", namespace = "http://control/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "conversor", namespace = "http://control/", propOrder = {
    "valor",
    "unidade"
})
public class Conversor {

    @XmlElement(name = "valor", namespace = "")
    private Double valor;
    @XmlElement(name = "unidade", namespace = "")
    private String unidade;

    /**
     * 
     * @return
     *     returns Double
     */
    public Double getValor() {
        return this.valor;
    }

    /**
     * 
     * @param valor
     *     the value for the valor property
     */
    public void setValor(Double valor) {
        this.valor = valor;
    }

    /**
     * 
     * @return
     *     returns String
     */
    public String getUnidade() {
        return this.unidade;
    }

    /**
     * 
     * @param unidade
     *     the value for the unidade property
     */
    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

}
