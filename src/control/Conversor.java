package control;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class Conversor {

	@WebMethod
	public String conversor (@WebParam(name = "valor")Double valor, @WebParam(name = "unidade")String unidade) {
		
		Double resultado = 0.0;
		
		// 'c' para Celcius e 'f' para Fahrenheit
		if (unidade.equals("c")) {
			resultado = (valor * (9.0/5.0)) + 32;
			resultado = Math.round(resultado*100.0)/100.0; // setando duas casas decimais
			return resultado.toString();
		} else if (unidade.equals("f")) {
			resultado = (valor - 32) * (5.0/9.0);
			resultado = Math.round(resultado*100.0)/100.0; // setando duas casas decimais
			return resultado.toString();
		} else {
			return "entrada inválida";
		}
		
	}

}
